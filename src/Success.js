import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'; 
import Button from 'react-bootstrap/Button';
import React, { useState, useRef, useEffect } from 'react';
import { FiCheckCircle } from "react-icons/fi";
import axios from 'axios';
import TestModule from './TestModule';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const Success = props => {
    return(
        <div className="pay-form">
            <div>
                <FiCheckCircle style={{
                    fontSize: "15vh",
                    color: "#4ca"
                }} />
            </div>
            <br></br>
            <h4>You've Paid</h4>
            <h3><b>$20.00 USD</b></h3>
            <p>to <i>ABC Shoe Online!</i></p>
            <br></br>
            <Link to={"/"}>
                <Button>Make Another Payment</Button>
            </Link>
        </div>
    )
}

export default Success;