import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'; 
import Button from 'react-bootstrap/Button';
import React, { useState, useRef, useEffect } from 'react';
import { FaCcVisa, FaCcMastercard, FaCcAmex } from 'react-icons/fa';
import axios from 'axios';
import TestModule from './TestModule';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import PayForm from "./PayForm";
import Verifying from "./Verifying";
import Success from "./Success";

function App () {

  return (
    <Container>
      <Router>
        <Row className="header">
          <Col>
          <img className="logo" src="https://s21.q4cdn.com/254933054/files/images/GlobalPayments_Symbol_Wordmark_REV.png" alt="Global Payments"/>
          </Col>
          <Col style={{
            justifyContent: "flex-end"
          }}>
            <h2>MiGS Payment</h2>
          </Col>
        </Row>
        <Switch>
          <Route path="/" exact>
            <PayForm />
          </Route>
          <Route path="/verifying/:cardType" exact>
            <Verifying />
          </Route>
          <Route path="/success" exact>
            <Success />
          </Route>
          <Redirect to="/" />
        </Switch>
      </Router>
    </Container>
  )
}

export default App;