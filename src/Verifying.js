import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form'; 
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';
import React, { useState, useRef, useEffect } from 'react';
import { FaCcVisa, FaCcMastercard, FaCcAmex } from 'react-icons/fa';
import axios from 'axios';
import TestModule from './TestModule';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
  useParams
} from "react-router-dom";

const Verifying = props => {
    const endTime = Math.floor(Math.random() * 5)+3;
    const [count, setCount] = useState(0);

    useEffect(() => {
        const timer = setTimeout(() => {
            setCount(count+1);
        }, 1000);
        return () => clearTimeout(timer);
    })
    console.log(useParams().cardType);
    const cardType = useParams().cardType;
    let authHostImg = "";
    if(cardType === "4"){
        authHostImg = "https://www.3dsecure.io/static/5f1d11f9ecbf13f0856cbe36736477d5/bc8e0/visa-logo.png";
    }else if(cardType === "2" || cardType === "5"){
        authHostImg = "https://www.3dsecure.io/static/a8bbe8200fe5371b6655d2785895fdd3/93212/mcidcheck.png";
    }else if(cardType === "3"){
        authHostImg = "https://www.3dsecure.io/static/133d42e107afdc4ccd35d07dbbc8012b/bc8e0/AMEX.png";
    }else{
        authHostImg = "https://www.nicepng.com/png/full/339-3393767_3d-secure-logo-png.png";
    };

    return(
        <div className="pay-form" style={{
            flexDirection: "column",
        }}>
                <h4><Spinner animation="grow" variant="primary" /> Verifying with your Bank </h4>
                
                <br></br>
                <p>Powered by:</p>
                <p>
                    <img className="authImg" src={authHostImg} />
                </p>
                {count >= endTime && <Redirect to="/success" />}
        </div>
    )
}

export default Verifying;